set spelllang=en_us
set encoding=utf-8 nobomb

" ########## PLUGINS ##########
call plug#begin('~/.local/share/nvim/plugged')

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'arcticicestudio/nord-vim'

if has('nvim')
	Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
	Plug 'Shougo/deoplete.nvim'
	Plug 'roxma/nvim-yarp'
	Plug 'roxma/vim-hug-neovim-rpc'
endif

Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'ludovicchabant/vim-gutentags'
Plug 'fatih/vim-go'
Plug 'machakann/vim-highlightedyank'
Plug 'tpope/vim-fugitive'
Plug 'jiangmiao/auto-pairs'
let g:deoplete#enable_at_startup = 1

call plug#end()

" ########## APPEARANCE ##########
colorscheme nord

let g:nord_comment_brightness = 20

syntax on
filetype plugin indent on

set backupdir=~/.vim/backups
set directory=~/.vim/swaps

set hidden
set lazyredraw
set ttyfast

set history=999
set undolevels=999
set autoread

set hlsearch
set incsearch
set ignorecase smartcase

set cursorline
set number
set report=0
set showmode
set showcmd
set showmatch
set splitbelow splitright
set title
set scrolloff=5
set sidescrolloff=7
set sidescroll=1

if has('title') && (has('gui_running') || &title)
	set titlestring=
	set titlestring+=%f
	set titlestring+=%h%m%r%w
	set titlestring+=\ -\ %{v:progname}
	set titlestring+=\ -\ %{substitute(getcwd(),\ $HOME,\ '~',\ '')}
endif

set wildmenu
set wildchar=<TAB>
set wildmode=list:longest
set wildignore+=*.DS_STORE,*.db,node_modules/**,*.jpg,*.png,*.gif

set diffopt=filler
set diffopt+=iwhite

set visualbell

set autoindent smartindent
set copyindent
set softtabstop=2
set tabstop=2
set shiftwidth=2
set smarttab

set textwidth=80

if has('clipboard')
	if has('unnamedplus')
		set clipboard=unnamed,unnamedplus
	else
		set clipboard=unnamed
	endif
endif

" ########## CONFIGURATION ##########
let g:netrw_liststyle = 3
let g:netrw_browse_split = 3

" ########## KEY BINDINGS ##########
let mapleader=","
let maplocalleader=";"

nnoremap <LEADER>te :tabedit
nnoremap <LEADER>tp :tabp<CR>
nnoremap <LEADER>tn :tabn<CR>

nnoremap <M-PageUp> :tabp<CR>
nnoremap <M-PageDown> :tabn<CR>

nnoremap <LEADER>vc :tabedit $MYVIMRC<CR>
nnoremap <LEADER>vs :source $MYVIMRC<CR>

nnoremap <LEADER>ev :Vexplore<CR>
nnoremap <LEADER>es :Sexplore<CR>
nnoremap <LEADER>ee :Explore<CR>

nnoremap <LOCALLEADER>ie :GoIfErr<CR>O
